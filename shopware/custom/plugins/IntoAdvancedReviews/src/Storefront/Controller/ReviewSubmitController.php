<?php declare(strict_types=1);

namespace IntoAdvancedReviews\Storefront\Controller;

use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\Validation\DataBag\RequestDataBag;
use Shopware\Storefront\Controller\StorefrontController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Shopware\Core\Framework\Uuid\Uuid;


/**
 * @Route(defaults={"_routeScope"={"storefront"}})
 */
class ReviewSubmitController extends StorefrontController
{
    private EntityRepository $reviewRepository;

    public function __construct(EntityRepository $reviewRepository)
    {
        $this->reviewRepository = $reviewRepository;
    }

    /**
     * @Route("/advanced-review/submit/{productId}", name="frontend.into_advanced_reviews.submit", options={"seo"="false"}, methods={"POST"})
     */
    public function submitReview(string $productId, RequestDataBag $data, SalesChannelContext $context): Response
    {
        $headline = $data->get('headline');
        $text = $data->get('text');
        $name = $context->getCustomer()->getFirstName();
        $email = $context->getCustomer()->getEmail();
        $rating = $data->getInt('rating');
        $alias = $data->get('alias');
        $media = $data->get('media');
        $wordFilter = $data->getBoolean('wordFilter');
        $isApproved = $data->getBoolean('isApproved');
        $totalInteractions = $data->getInt('totalInteractions');

        $reviewData = [
            'productId' => $productId,
            'customerId' => $context->getCustomer()->getId(),
            'languageId' => $context->getContext()->getLanguageId(),
            'headline' => $headline,
            'text' => $text,
            'name' => $name,
            'email' => $email,
            'rating' => $rating,
            'alias' => $alias,
            'media' => $media,
            'wordFilter' => $wordFilter,
            'isApproved' => $isApproved,
            'totalInteractions' => $totalInteractions,
        ];

        $this->reviewRepository->create([$reviewData], $context->getContext());

        return $this->redirectToRoute('frontend.detail.page', ['productId' => $productId]);
    }
}