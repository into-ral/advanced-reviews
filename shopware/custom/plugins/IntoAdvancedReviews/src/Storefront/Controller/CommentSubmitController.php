<?php declare(strict_types=1);

namespace IntoAdvancedReviews\Storefront\Controller;

use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\Validation\DataBag\RequestDataBag;
use Shopware\Storefront\Controller\StorefrontController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Shopware\Core\System\SalesChannel\SalesChannelContext;

/**
 * @Route(defaults={"_routeScope"={"storefront"}})
 */
class CommentSubmitController extends StorefrontController
{
    private EntityRepository $commentRepository;
    private EntityRepository $reviewRepository;

    public function __construct(EntityRepository $commentRepository, EntityRepository $reviewRepository)
    {
        $this->commentRepository = $commentRepository;
        $this->reviewRepository = $reviewRepository;
    }

    /**
     * @Route("/advanced-review/comment/submit/{reviewId}", name="frontend.into_advanced_reviews.comment.submit_to_review", options={"seo"="false"}, methods={"POST"})
     */
    public function submitCommentToReview(string $reviewId, RequestDataBag $data, SalesChannelContext $context): Response
    {
        $text = $data->get('text');
        $name = $data->get('alias');
        $media = $data->get('media');
        $isApproved = $data->getBoolean('isApproved');
        $email = $context->getCustomer()->getEmail();
        $totalInteractions = $data->getInt('totalInteractions');

        $commentData = [
            'reviewId' => $reviewId,
            'customerId' => $context->getCustomer()->getId(),
            'languageId' => $context->getContext()->getLanguageId(),
            'text' => $text,
            'media' => $media,
            'isApproved' => $isApproved,
            'name' => $name,
            'email' => $email,
            'totalInteractions' => $totalInteractions,
        ];

        $this->commentRepository->create([$commentData], $context->getContext());

        $productId = $this->getProductIdFromReviewId($reviewId, $context);
        return $this->redirectToRoute('frontend.detail.page', ['productId' => $productId]);
    }

    private function getProductIdFromReviewId(string $reviewId, SalesChannelContext $context): ?string
    {
        $criteria = new Criteria([$reviewId]);
        $criteria->addAssociation('product');

        $review = $this->reviewRepository->search($criteria, $context->getContext())->first();

        return $review ? $review->getProductId() : null;
    }

    /**
     * @Route("/advanced-review/comment/submit-to-comment/{commentId}", name="frontend.into_advanced_reviews.comment.submit_to_comment", options={"seo"="false"}, methods={"POST"})
     */
    public function submitCommentToComment(string $commentId, RequestDataBag $data, SalesChannelContext $context): Response
    {
        $text = $data->get('text');
        $name = $data->get('alias');
        $email = $context->getCustomer()->getEmail();
        $media = $data->get('media');
        $isApproved = $data->getBoolean('isApproved');
        $totalInteractions = $data->getInt('totalInteractions');
        $reviewId = $this->getReviewIdFromCommentId($commentId, $context);

        $commentData = [
            'parentId' => $commentId,
            'customerId' => $context->getCustomer()->getId(),
            'languageId' => $context->getContext()->getLanguageId(),
            'reviewId' => $reviewId,
            'referenceCommentId' => $commentId,
            'text' => $text,
            'media' => $media,
            'isApproved' => $isApproved,
            'name' => $name,
            'email' => $email,
            'totalInteractions' => $totalInteractions,
        ];

        $this->commentRepository->create([$commentData], $context->getContext());

        $productId = $this->getProductIdFromCommentId($commentId, $context);
        return $this->redirectToRoute('frontend.detail.page', ['productId' => $productId]);
    }

    private function getReviewIdFromCommentId(string $commentId, SalesChannelContext $context): ?string
    {
        $criteria = new Criteria([$commentId]);
        $criteria->addAssociation('review');

        $comment = $this->commentRepository->search($criteria, $context->getContext())->first();

        return $comment && $comment->getReview() ? $comment->getReview()->getId() : null;
    }
    private function getProductIdFromCommentId(string $commentId, SalesChannelContext $context): ?string
    {
        $criteria = new Criteria([$commentId]);
        $criteria->addAssociation('review.product');

        $comment = $this->commentRepository->search($criteria, $context->getContext())->first();

        return $comment && $comment->getReview() ? $comment->getReview()->getProductId() : null;
    }
}