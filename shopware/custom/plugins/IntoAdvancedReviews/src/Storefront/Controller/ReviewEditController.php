<?php declare(strict_types=1);

namespace IntoAdvancedReviews\Storefront\Controller;

use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\MultiFilter;
use Shopware\Core\Framework\Validation\DataBag\RequestDataBag;
use Shopware\Storefront\Controller\StorefrontController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Shopware\Core\System\SalesChannel\SalesChannelContext;

/**
 * @Route(defaults={"_routeScope"={"storefront"}})
 */
class ReviewEditController extends StorefrontController
{
    private EntityRepository $reviewRepository;
    private \Twig\Environment $twig;

    public function __construct(EntityRepository $reviewRepository, \Twig\Environment $twig)
    {
        $this->reviewRepository = $reviewRepository;
        $this->twig = $twig;
    }

    /**
     * @Route("/advanced-review/review/edit/{reviewId}", name="frontend.into_advanced_reviews.review.edit_form", options={"seo"="false"}, methods={"GET"})
     */
    public function showEditReviewForm(string $reviewId, SalesChannelContext $context): Response
    {
        $criteria = new Criteria([$reviewId]);
        $review = $this->reviewRepository->search($criteria, $context->getContext())->first();

        if (!$review) {
            throw $this->createNotFoundException('Review not found');
        }

        if ($review->getCustomerId() !== $context->getCustomer()->getId()) {
            throw $this->createAccessDeniedException('You are not allowed to edit this review');
        }

        return new Response($this->twig->render('@IntoAdvancedReviews/storefront/page/product-detail/review/edit_review_form.html.twig', [
            'review' => $review,
        ]));
    }

    public function canEditReview(string $reviewId, string $customerId, SalesChannelContext $context): bool
    {
        $criteria = new Criteria();
        $criteria->addFilter(new MultiFilter(MultiFilter::CONNECTION_AND, [
            new EqualsFilter('id', $reviewId),
            new EqualsFilter('customerId', $customerId),
        ]));

        $reviews = $this->reviewRepository->search($criteria, $context->getContext());

        return $reviews->count() > 0;
    }

    /**
     * @Route("/advanced-review/review/edit/{reviewId}", name="frontend.into_advanced_reviews.review.edit", options={"seo"="false"}, methods={"POST"})
     */
    public function editReview(string $reviewId, RequestDataBag $data, SalesChannelContext $context): Response
    {
        $customerId = $context->getCustomer()->getId();

        if (!$this->canEditReview($reviewId, $customerId, $context)) {
            return $this->json(['error' => 'Unauthorized'], Response::HTTP_FORBIDDEN);
        }

        $headline = $data->get('headline');
        $text = $data->get('text');
        $rating = $data->getInt('rating');

        $reviewData = [
            'id' => $reviewId,
            'headline' => $headline,
            'text' => $text,
            'rating' => $rating,
        ];

        $this->reviewRepository->update([$reviewData], $context->getContext());

        return $this->redirectToRoute('frontend.detail.page', ['productId' => $data->get('productId')]);
    }

    /**
     * @Route("/advanced-review/review/delete/{reviewId}", name="frontend.into_advanced_reviews.review.delete", options={"seo"="false"}, methods={"POST"})
     */
    public function deleteReview(string $reviewId, SalesChannelContext $context): Response
    {
        $customerId = $context->getCustomer()->getId();

        if (!$this->canEditReview($reviewId, $customerId, $context)) {
            return $this->json(['error' => 'Unauthorized'], Response::HTTP_FORBIDDEN);
        }

        $criteria = new Criteria([$reviewId]);
        $review = $this->reviewRepository->search($criteria, $context->getContext())->first();
        $productId = $review->getProductId();

        $this->reviewRepository->delete([['id' => $reviewId]], $context->getContext());

        if ($productId) {
            return $this->redirectToRoute('frontend.detail.page', ['productId' => $productId]);
        } else {
            return $this->redirectToRoute('frontend.home.page');
        }
    }
}