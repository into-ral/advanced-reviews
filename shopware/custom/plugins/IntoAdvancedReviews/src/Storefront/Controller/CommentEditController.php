<?php declare(strict_types=1);

namespace IntoAdvancedReviews\Storefront\Controller;

use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\MultiFilter;
use Shopware\Core\Framework\Validation\DataBag\RequestDataBag;
use Shopware\Storefront\Controller\StorefrontController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Shopware\Core\System\SalesChannel\SalesChannelContext;

/**
 * @Route(defaults={"_routeScope"={"storefront"}})
 */
class CommentEditController extends StorefrontController
{
    private EntityRepository $reviewRepository;
    private EntityRepository $commentRepository;
    private \Twig\Environment $twig;

    public function __construct(EntityRepository $reviewRepository, EntityRepository $commentRepository, \Twig\Environment $twig)
    {
        $this->reviewRepository = $reviewRepository;
        $this->commentRepository = $commentRepository;
        $this->twig = $twig;
    }

    /**
     * @Route("/advanced-review/comment/edit/{commentId}", name="frontend.into_advanced_reviews.comment.edit_form", options={"seo"="false"}, methods={"GET"})
     */
    public function showEditCommentForm(string $commentId, SalesChannelContext $context): Response
    {
        $criteria = new Criteria([$commentId]);
        $comment = $this->commentRepository->search($criteria, $context->getContext())->first();

        if (!$comment) {
            throw $this->createNotFoundException('Comment not found');
        }

        if ($comment->getCustomer() !== $context->getCustomer()->getId()) {
            throw $this->createAccessDeniedException('You are not allowed to edit this comment');
        }

        return new Response($this->twig->render('@IntoAdvancedReviews/storefront/page/product-detail/comment/edit_comment_form.html.twig', [
            'comment' => $comment,
        ]));
    }

    public function canEditComment(string $commentId, string $customerId, SalesChannelContext $context): bool
    {
        $criteria = new Criteria();
        $criteria->addFilter(new MultiFilter(MultiFilter::CONNECTION_AND, [
            new EqualsFilter('id', $commentId),
            new EqualsFilter('customerId', $customerId),
        ]));

        $comments = $this->commentRepository->search($criteria, $context->getContext());

        return $comments->count() > 0;
    }

    /**
     * @Route("/advanced-review/comment/edit/{commentId}", name="frontend.into_advanced_reviews.comment.edit", options={"seo"="false"}, methods={"POST"})
     */
    public function editComment(string $commentId, RequestDataBag $data, SalesChannelContext $context): Response
    {
        $customerId = $context->getCustomer()->getId();

        if (!$this->canEditComment($commentId, $customerId, $context)) {
            return $this->json(['error' => 'Unauthorized'], Response::HTTP_FORBIDDEN);
        }

        $text = $data->get('text');
        $alias = $data->get('alias');

        $commentData = [
            'id' => $commentId,
            'name' => $alias,
            'text' => $text,
        ];

        $productId = $this->getProductIdFromCommentId($commentId, $context);

        $this->commentRepository->update([$commentData], $context->getContext());

        return $this->redirectToRoute('frontend.detail.page', ['productId' => $productId]);
    }

    private function getProductIdFromCommentId(string $commentId, SalesChannelContext $context): ?string
    {
        $criteria = new Criteria([$commentId]);
        $criteria->addAssociation('review.product');

        $comment = $this->commentRepository->search($criteria, $context->getContext())->first();

        return $comment && $comment->getReview() ? $comment->getReview()->getProductId() : null;
    }

    /**
     * @Route("/advanced-review/comment/delete/{commentId}", name="frontend.into_advanced_reviews.comment.delete", options={"seo"="false"}, methods={"POST"})
     */
    public function deleteComment(string $commentId, SalesChannelContext $context): Response
    {
        $customerId = $context->getCustomer()->getId();

        if (!$this->canEditComment($commentId, $customerId, $context)) {
            return $this->json(['error' => 'Unauthorized'], Response::HTTP_FORBIDDEN);
        }
        $productId = $this->getProductIdFromCommentId($commentId, $context);

        $this->commentRepository->delete([['id' => $commentId]], $context->getContext());

        return $this->redirectToRoute('frontend.detail.page', ['productId' => $productId]);
    }
}
