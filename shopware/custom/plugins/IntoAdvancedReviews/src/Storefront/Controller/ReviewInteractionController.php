<?php declare(strict_types=1);

namespace IntoAdvancedReviews\Storefront\Controller;

use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\MultiFilter;
use Shopware\Core\Framework\Validation\DataBag\RequestDataBag;
use Shopware\Storefront\Controller\StorefrontController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Shopware\Core\Framework\Uuid\Uuid;

/**
 * @Route(defaults={"_routeScope"={"storefront"}})
 */
class ReviewInteractionController extends StorefrontController
{
    private EntityRepository $reviewRepository;
    private EntityRepository $commentRepository;
    private EntityRepository $interactionRepository;

    public function __construct(EntityRepository $reviewRepository, EntityRepository $commentRepository, EntityRepository $interactionRepository)
    {
        $this->reviewRepository = $reviewRepository;
        $this->commentRepository = $commentRepository;
        $this->interactionRepository = $interactionRepository;
    }

    /**
     * @Route("/advanced-review/review-interaction/{reviewId}", name="frontend.into_advanced_reviews.interaction", options={"seo"="false"}, methods={"POST"})
     */
    public function submitReviewInteraction(string $reviewId, RequestDataBag $data, SalesChannelContext $context): Response
    {
        return $this->handleInteraction($reviewId, $data, $context, 'review');
    }

    /**
     * @Route("/advanced-review/comment-interaction/{commentId}", name="frontend.into_advanced_reviews.comment_interaction", options={"seo"="false"}, methods={"POST"})
     */
    public function submitCommentInteraction(string $commentId, RequestDataBag $data, SalesChannelContext $context): Response
    {
        return $this->handleInteraction($commentId, $data, $context, 'comment');
    }

    private function handleInteraction(string $id, RequestDataBag $data, SalesChannelContext $context, string $interactionType): JsonResponse
    {
        if ($context->getCustomer() == null) {
            return new JsonResponse(['message' => 'Bitte loggen Sie sich zuerst ein, um ein Voting abzugeben!']);
        }

        $customerId = $context->getCustomer()->getId();
        $isHelpful = $data->getBoolean('isHelpful');

        $message = '';

        // Check if an interaction already exists
        $existingInteractionCriteria = (new Criteria())->addFilter(new MultiFilter(MultiFilter::CONNECTION_AND, [
            new EqualsFilter($interactionType . 'Id', $id),
            new EqualsFilter('customerId', $customerId)
        ]));

        $existingInteraction = $this->interactionRepository->search($existingInteractionCriteria, $context->getContext());

        if ($existingInteraction->count() > 0) {
            // An interaction already exists - check if its the same as the new one
            $existingIsHelpful = $existingInteraction->first()->get('isHelpful');
            if ($existingIsHelpful === $isHelpful) {
                return new JsonResponse(['message' => 'Sie haben bereits abgestimmt']);
            } else {
                // The existing interaction is different from the new one - update it
                $interactionId = $existingInteraction->first()->getId();
                $this->interactionRepository->update([[
                    'id' => $interactionId,
                    'isHelpful' => $isHelpful,
                ]], $context->getContext());

                // Adjust the total interactions count
                $adjustment = $isHelpful ? 2 : -2;

                $message = 'Interaktion erfolgreich verändert!';
            }
        } else {
            // No interaction exists - create a new one
            $interactionData = [
                'id' => Uuid::randomHex(),
                $interactionType . 'Id' => $id,
                'customerId' => $customerId,
                'isHelpful' => $isHelpful,
            ];
            if ($interactionType === 'comment') {
                $interactionData['reviewId'] = $data->get('reviewId')->get('reviewCommentId');
            }

            $this->interactionRepository->create([$interactionData], $context->getContext());

            $adjustment = $isHelpful ? 1 : -1;

            $message = 'Interaktion erfolgreich hinzugefügt!';
        }

        $repository = $interactionType === 'review' ? $this->reviewRepository : $this->commentRepository;
        $entity = $repository->search(new Criteria([$id]), $context->getContext())->first();

        $currentTotalInteractions = $entity->get('totalInteractions');

        $newTotalInteractions = $currentTotalInteractions + $adjustment;

        $repository->update([[
            'id' => $id,
            'totalInteractions' => $newTotalInteractions,
        ]], $context->getContext());

        return new JsonResponse(['message' => $message]);
    }
}