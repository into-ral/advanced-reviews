<?php declare(strict_types=1);

namespace IntoAdvancedReviews\Migration;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Exception;
use Shopware\Core\Framework\Migration\MigrationStep;

class Migration1683812110CreateInteractionTable extends MigrationStep
{
    public function getCreationTimestamp(): int
    {
        return 1683812110;
    }

    /**
     * @throws Exception
     */
    public function update(Connection $connection): void
    {
        // Create table for interaction_entity
        $connection->executeStatement('
        CREATE TABLE IF NOT EXISTS `into_advanced_product_review_interaction` (
            `id` BINARY(16) NOT NULL,
            `review_id` BINARY(16) DEFAULT NULL,
            `comment_id` BINARY(16) DEFAULT NULL,
            `customer_id` BINARY(16) NOT NULL,
            `is_helpful` TINYINT(1) NOT NULL,
            `created_at` DATETIME(3) NOT NULL,
            `updated_at` DATETIME(3) DEFAULT NULL,
            PRIMARY KEY (`id`),
            CONSTRAINT `fk.into_advanced_product_review_interaction_review_id` FOREIGN KEY (`review_id`) REFERENCES `into_advanced_product_review` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
            CONSTRAINT `fk.into_advanced_product_review_interaction_customer_id` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
            CONSTRAINT `fk.into_advanced_product_review_interaction_comment_id` FOREIGN KEY (`comment_id`) REFERENCES `into_advanced_product_review_comment` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
    ');
    }

    public function updateDestructive(Connection $connection): void {

    }
}
