<?php declare(strict_types=1);

namespace IntoAdvancedReviews\Migration;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Exception;
use Shopware\Core\Framework\Migration\MigrationStep;

class Migration1683811993CreateReviewTable extends MigrationStep
{
    public function getCreationTimestamp(): int
    {
        return 1683811993;
    }

    /**
     * @throws Exception
     */
    public function update(Connection $connection): void
    {
        // Create table for review_entity
        $connection->executeStatement('
        CREATE TABLE IF NOT EXISTS `into_advanced_product_review` (
            `id` BINARY(16) NOT NULL,
            `product_id` BINARY(16) NOT NULL,
            `customer_id` BINARY(16) NOT NULL,
            `language_id` BINARY(16) NOT NULL,
            `migrated_product_id` INT(11),
            `migrated_review_id` INT(11),
            `rating` INT(11) NOT NULL,
            `alias` VARCHAR(255) NOT NULL,
            `headline` VARCHAR(255) NOT NULL,
            `text` LONGTEXT NOT NULL,
            `total_interactions` INT DEFAULT 0,
            `media` VARCHAR(255) DEFAULT NULL,
            `is_approved` TINYINT(1) DEFAULT 0,
            `word_filter` TINYINT(1) DEFAULT 0,
            `created_at` DATETIME(3) NOT NULL,
            `updated_at` DATETIME(3) DEFAULT NULL,
            PRIMARY KEY (`id`),
            CONSTRAINT `fk.into_advanced_product_review.product_id` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
            CONSTRAINT `fk.into_advanced_product_review_interaction.customer_id` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
            CONSTRAINT `fk.into_advanced_product_review.language_id` FOREIGN KEY (`language_id`) REFERENCES `language` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
    ');
    }

    /**
     * @throws Exception
     */
    public function updateDestructive(Connection $connection): void
    {
        $connection->executeStatement('
        DROP TABLE IF EXISTS `into_advanced_product_review_interaction`;
        DROP TABLE IF EXISTS `into_advanced_product_review_comment`;
        DROP TABLE IF EXISTS `into_advanced_product_review`;
    ');
    }
}
