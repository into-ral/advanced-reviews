<?php declare(strict_types=1);

namespace IntoAdvancedReviews\Migration;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Exception;
use Shopware\Core\Framework\Migration\MigrationStep;

class Migration1683812077CreateCommentTable extends MigrationStep
{
    public function getCreationTimestamp(): int
    {
        return 1683812077;
    }

    /**
     * @throws Exception
     */
    public function update(Connection $connection): void
    {
        // Create table for comment_entity
        $connection->executeStatement('
        CREATE TABLE IF NOT EXISTS `into_advanced_product_review_comment` (
            `id` BINARY(16) NOT NULL,
            `review_id` BINARY(16) NOT NULL,
            `customer_id` BINARY(16) NOT NULL,
            `reference_comment_id` BINARY(16) DEFAULT NULL,
            `migrated_comment_id` INT(11) DEFAULT NULL,
            `name` VARCHAR(255) NOT NULL,
            `email` VARCHAR(255) NOT NULL,
            `text` LONGTEXT NOT NULL,
            `total_interactions` INT NOT NULL,
            `is_approved` TINYINT(1) NOT NULL,
            `media` VARCHAR(255) DEFAULT NULL,
            `created_at` DATETIME(3) NOT NULL,
            `updated_at` DATETIME(3) DEFAULT NULL,
            PRIMARY KEY (`id`),
            CONSTRAINT `fk.into_advanced_product_review_comment.review_id` FOREIGN KEY (`review_id`) REFERENCES `into_advanced_product_review` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
            CONSTRAINT `fk.into_advanced_product_review_comment.customer_id` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
            CONSTRAINT `fk.into_advanced_product_review_comment.reference_comment_id` FOREIGN KEY (`reference_comment_id`) REFERENCES `into_advanced_product_review_comment` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
    ');
    }

    public function updateDestructive(Connection $connection): void {

    }
}
