import deDE from './snippet/de-DE';
import enGB from './snippet/en-GB';

const { Module } = Shopware;

Shopware.Component.register("into-review-list", () => import("./page/into-review-list"))
Shopware.Component.register("into-review-detail", () => import("./page/into-review-detail"))

Module.register('into-advanced-reviews', {
    type: 'plugin',
    name: 'Example',
    title: 'into-advanced-reviews.navigation.label',
    description: 'sw-property.general.descriptionTextModule',
    color: '#ff3d58',
    icon: 'default-shopping-paper-bag-product',

    snippets: {
        'de-DE': deDE,
        'en-GB': enGB
    },

    routes: {
        list: {
            component: 'into-review-list',
            path: 'list'
        },

        detail: {
            component: 'into-review-detail',
            path: 'detail/:id',
            meta: {
                parentPath: 'into.advanced.reviews.list'
            }
        },

        create: {
            component: 'into-advanced-reviews-create',
            path: 'create',
            meta: {
                parentPath: 'into.advanced.reviews.list'
            }
        }
    },

    navigation: [{
        label: 'into-advanced-reviews.navigation.label',
        color: '#ff3d58',
        path: 'into.advanced.reviews.list',
        icon: 'default-shopping-paper-bag-product',
        position: 100,
        parent: 'sw-catalogue',
    }]
});