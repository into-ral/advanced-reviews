import template from './into-review-detail.html.twig';
import './into-review-detail.scss';

const { Criteria } = Shopware.Data;

/**
 * @package inventory
 */
export default {
    template,

    inject: ['repositoryFactory', 'acl', 'customFieldDataProviderService'],

    mixins: [
        'placeholder',
        'notification',
        'salutation',
    ],

    shortcuts: {
        'SYSTEMKEY+S': {
            active() {
                return this.acl.can('review.editor');
            },
            method: 'onSave',
        },
        ESCAPE: 'onCancel',
    },

    data() {
        return {
            isLoading: null,
            isSaveSuccessful: false,
            reviewId: null,
            review: {},
            customFieldSets: null,
        };
    },

    metaInfo() {
        return {
            title: this.$createTitle(this.identifier),
        };
    },

    computed: {
        identifier() {
            return this.review.title;
        },

        repository() {
            return this.repositoryFactory.create('into_advanced_product_review');
        },

        stars() {
            if (this.review.rating >= 0) {
                return this.review.rating;
            }

            return 0;
        },

        languageCriteria() {
            const criteria = new Criteria(1, 25);

            criteria.addSorting(Criteria.sort('name', 'ASC', false));

            return criteria;
        },

        tooltipSave() {
            if (!this.acl.can('review.editor')) {
                return {
                    message: this.$tc('sw-privileges.tooltip.warning'),
                    disabled: true,
                    showOnDisabledElements: true,
                };
            }

            const systemKey = this.$device.getSystemKey();

            return {
                message: `${systemKey} + S`,
                appearance: 'light',
            };
        },

        tooltipCancel() {
            return {
                message: 'ESC',
                appearance: 'light',
            };
        },

        showCustomFields() {
            return this.review && this.customFieldSets && this.customFieldSets.length > 0;
        },

        dateFilter() {
            return Shopware.Filter.getByName('date');
        },
    },

    watch: {
        '$route.params.id'() {
            this.createdComponent();
        },
    },

    created() {
        this.createdComponent();
    },

    methods: {
        createdComponent() {
            Shopware.ExtensionAPI.publishData({
                id: 'sw-review-detail__review',
                path: 'review',
                scope: this,
            });
            if (this.$route.params.id) {
                this.reviewId = this.$route.params.id;

                this.loadEntityData();
                this.loadCustomFieldSets();
            }
        },

        loadEntityData() {
            this.isLoading = true;
            const criteria = new Criteria(1, 25);
            criteria.addAssociation('customer');
            // criteria.addAssociation('salesChannel');
            criteria.addAssociation('product');
            criteria.addFilter(
                Criteria.equals('id', this.reviewId),
            );

            const context = { ...Shopware.Context.api, inheritance: true };

            this.repository.search(criteria, context).then((review) => {
                this.review = review.first();
                this.isLoading = false;
            });
        },

        loadCustomFieldSets() {
            this.customFieldDataProviderService.getCustomFieldSets('product_review').then((sets) => {
                this.customFieldSets = sets;
            });
        },

        onSave() {
            this.isSaveSuccessful = false;
            const messageSaveError = this.$tc(
                'global.notification.notificationSaveErrorMessageRequiredFieldsInvalid',
            );

            this.repository.save(this.review).then(() => {
                this.isSaveSuccessful = true;
            }).catch(() => {
                this.createNotificationError({
                    message: messageSaveError,
                });
            });
        },

        onSaveFinish() {
            this.loadEntityData();
            this.isSaveSuccessful = false;
        },

        onCancel() {
            this.$router.push({ name: 'into.advanced.reviews.list' });
        }
    },
};
