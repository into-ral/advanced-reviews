import template from './into-review-list.html.twig';
import './into-review-list.scss';

const {Component, Data, Context} = Shopware;
const {Criteria} = Data;
const {mapState, mapGetters} = Component.getComponentHelper();

export default {
    template,

    inject: [
        'repositoryFactory',
        'acl'
    ],

    data() {
        return {
            isLoading: false,
            items: null,
            sortBy: 'createdAt',
            term: '',
            page: 1,
            limit: 25,
        };
    },

    metaInfo() {
        return {
            title: this.$createTitle()
        };
    },

    computed: {
        columns() {
            return [
                {
                    property: 'headline',
                    dataIndex: 'headline',
                    label: 'sw-review.list.columnTitle',
                },
                {
                    property: 'rating',
                    dataIndex: 'rating',
                    label: 'sw-review.list.columnPoints',
                },
                {
                    property: 'isApproved',
                    dataIndex: 'isApproved',
                    label: 'sw-review.list.columnStatus',
                    align: 'center',
                    editable: true,
                },
                {
                    property: 'product',
                    dataIndex: 'product.name',
                    label: 'sw-review.list.columnProduct',
                    routerLink: 'into.advanced.reviews.detail',
                    primary: true,
                },
                {
                    property: 'alias',
                    dataIndex: 'customer.lastName,customer.firstName',
                    label: 'sw-review.list.columnUser',
                },
                {
                    property: 'createdAt',
                    dataIndex: 'createdAt',
                    label: 'sw-review.list.columnCreatedAt',
                },

                // {
                //     property: 'total_interactions',
                //     dataIndex: 'total_interactions',
                //     label: 'sw-review.list.columnComment',
                //     align: 'center',
                // },
            ];
        },
        repository() {
            return this.repositoryFactory.create('into_advanced_product_review');
        },
        criteria() {
            const criteria = new Criteria(this.page, this.limit);

            criteria.setTerm(this.term);

            this.sortBy.split(',').forEach(sorting => {
                criteria.addSorting(Criteria.sort(sorting, this.sortDirection, this.naturalSorting));
            });
            criteria.addAssociation('customer');
            criteria.addAssociation('product');

            return criteria;
        },
    },

    created() {
        this.createdComponent();
    },

    methods: {
        createdComponent() {
            this.getList();
        },

        onRefresh() {
            this.getList();
        },

        onPageChange(pageObj) {
            this.page = Number(pageObj.page);
            this.limit = Number(pageObj.limit);
            this.getList();
        },

        onSortColumn(column) {
            this.sortColumn = column; // Oder was auch immer Sie bei der Sortierung der Spalte ausführen möchten
        },

        getList() {
            this.isLoading = true;

            const context = {...Shopware.Context.api, inheritance: true};
            const criteria = new Criteria(this.page, this.limit);
            criteria.setTerm(this.term);
            this.sortBy.split(',').forEach(sorting => {
                criteria.addSorting(Criteria.sort(sorting, this.sortDirection, this.naturalSorting));
            });
            criteria.addAssociation('customer');
            criteria.addAssociation('product');

            console.log("Criteria: ", criteria);

            return this.repository.search(criteria, context).then((result) => {
                console.log('Fetched data:', result);  // Log the fetched data
                this.total = result.total;
                this.items = result;
                this.isLoading = false;
            }).catch((error) => {
                console.error("Error: ", error);
                if (error.response && error.response.data) {
                    console.error("Error data: ", error.response.data);
                }
            });
        },

        toggleApproval(review) {
            review.isApproved = !review.isApproved;
            this.repository.save(review, Context.api).then(() => {
                this.getList();
            });
        },

        formatApprovalStatus(isApproved) {
            return isApproved ? 'Ja' : 'Nein';
        },

        onDelete(option) {
            this.$refs.listing.deleteItem(option);

            this.repository.search(this.criteria, {...Shopware.Context.api, inheritance: true}).then((result) => {
                this.total = result.total;
                this.items = result;
            });
        },
    },
}