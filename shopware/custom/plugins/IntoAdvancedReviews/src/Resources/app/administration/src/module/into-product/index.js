import './page/into-product-detail';
import './view/into-product-detail-custom-review';

import deDE from './snippet/de-DE';
import enGB from './snippet/en-GB';

Shopware.Module.register('into-product-detail-custom-review', {
    snippets: {
        'de-DE': deDE,
        'en-GB': enGB
    },

    routeMiddleware(next, currentRoute) {
        const customRouteName = 'into.product.detail.review';

        if (
            currentRoute.name === 'sw.product.detail'
            && currentRoute.children.every((currentRoute) => currentRoute.name !== customRouteName)
        ) {
            currentRoute.children.push({
                name: customRouteName,
                path: '/into/product/detail/:id/review',
                component: 'into-product-detail-custom-review',
                meta: {
                    parentPath: 'sw.product.index'
                },
                props: (route) => {
                    return {
                        product: route.params.product
                    };
                },
            });

            currentRoute.children.push({
                name: 'into.advanced.reviews.detail.custom',
                path: '/detail/review/:id',
                component: 'into-product-detail-review-tab-overview',
            });
        }
        next(currentRoute);
    }
});