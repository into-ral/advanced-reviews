import template from './into-product-detail-custom-review.html.twig';
import './into-product-detail-custom-review.scss';

const {Component, Data, Context} = Shopware;
const {Criteria} = Data;
const {mapState, mapGetters} = Component.getComponentHelper();


Component.register('into-product-detail-custom-review', {
    template,

    inject: [
        'repositoryFactory',
        'acl'
    ],

    data() {
        return {
            reviews: [],
            dataSource: [],
            showReviewDeleteModal: false,
            deleteReviewId: null,
            page: 1,
            limit: 25,
            total: 0,
        };
    },

    computed: {
        ...mapState('swProductDetail', [
            'product',
        ]),

        ...mapGetters('swProductDetail', [
            'isLoading',
        ]),

        cardTitle() {
            return this.total ? this.$tc('sw-product.reviews.cardTitleReviews') : null;
        },

        reviewRepository() {
            return this.repositoryFactory.create('into_advanced_product_review');
        },

        reviewCriteria() {
            const criteria = new Criteria(this.page, this.limit);

            criteria.addFilter(
                Criteria.equals('productId', this.product.id),
            );
            criteria.setTotalCountMode(1);

            return criteria;
        },

        reviewColumns() {
            return [
                {
                    property: 'alias',
                    dataIndex: 'alias',
                    label: this.$tc('sw-product.reviews.columnTitle.alias'),
                },
                {
                    property: 'rating',
                    dataIndex: 'rating',
                    label: this.$tc('sw-product.reviews.columnTitle.rating'),
                    align: 'center',
                },
                {
                    property: 'headline',
                    dataIndex: 'headline',
                    label: this.$tc('sw-product.reviews.columnTitle.headline'),
                },
                {
                    property: 'isApproved',
                    dataIndex: 'isApproved',
                    label: this.$tc('sw-product.reviews.columnTitle.isApproved'),
                },
            ];
        },
    },

    watch: {
        'product.id': {
            immediate: true,
            handler(newValue) {
                if (!newValue) {
                    return;
                }

                this.getReviews();
            },
        },
    },

    created() {
        this.createdComponent();
    },

    methods: {
        createdComponent() {
            this.getReviews();
        },

        getReviews() {
            if (!this.product || !this.product.id) {
                return;
            }

            this.reviewRepository.search(this.reviewCriteria, Context.api).then((reviews) => {
                this.total = reviews.total;
                this.dataSource = reviews;
                console.log("Datasource lautet: ");
                console.log(this.dataSource);

                if (this.total > 0 && this.dataSource.length <= 0) {
                    this.page = (this.page === 1) ? 1 : this.page - 1;
                    this.getReviews();
                }
            });
        },

        onStartReviewDelete(review) {
            this.deleteReviewId = review.id;
            this.onShowReviewDeleteModal();
        },

        onCancelReviewDelete() {
            this.deleteReviewId = null;
            this.onCloseReviewDeleteModal();
        },

        onShowReviewDeleteModal() {
            this.showReviewDeleteModal = true;
        },

        onCloseReviewDeleteModal() {
            this.showReviewDeleteModal = false;
        },

        onConfirmReviewDelete() {
            this.onCloseReviewDeleteModal();

            this.reviewRepository.delete(this.deleteReviewId, Context.api).then(() => {
                this.deleteReviewId = null;
                this.getReviews();
            });
        },

        onChangePage(data) {
            this.page = data.page;
            this.limit = data.limit;
            this.getReviews();
        },

        toggleApproval(review) {
            review.isApproved = !review.isApproved;
            this.reviewRepository.save(review, Context.api).then(() => {
                this.getReviews();
            });
        },
    },
});