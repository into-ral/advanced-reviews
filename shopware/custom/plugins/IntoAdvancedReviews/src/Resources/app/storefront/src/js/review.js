function handleReviewFormDisplay() {
    console.log('Review script loaded');

    var reviewButton = document.querySelector('.review-button');
    if (reviewButton) {
        reviewButton.addEventListener('click', function () {
            var form = document.getElementById('advanced-review-form');
            if (form.style.display === 'none' || form.style.display === '') {
                form.style.display = 'block';
            } else {
                form.style.display = 'none';
            }
        });
    }
}

function handleEditReviewFormDisplay() {
    console.log('Edit Review Script loaded');
    var editReviewButtons = document.querySelectorAll('.edit-review-button');
    editReviewButtons.forEach(function (button) {
        button.addEventListener('click', function () {
            var reviewId = this.dataset.reviewId;
            var form = document.getElementById('edit-review-form-' + reviewId);
            if (form.style.display === 'none' || form.style.display === '') {
                form.style.display = 'block';
            } else {
                form.style.display = 'none';
            }
        });
    });
}

document.addEventListener('DOMContentLoaded', function () {
    handleReviewFormDisplay();
    handleEditReviewFormDisplay();
});