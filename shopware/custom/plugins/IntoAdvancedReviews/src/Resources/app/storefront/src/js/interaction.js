function handleReviewInteraction() {
    var interactionButtons = document.querySelectorAll('.review-interaction-button');
    interactionButtons.forEach(function (button) {
        button.addEventListener('click', function () {

            var reviewId = this.dataset.reviewId;
            var isHelpful = this.dataset.isHelpful === '1';

            var xhr = new XMLHttpRequest();
            xhr.open('POST', '/advanced-review/review-interaction/' + reviewId);
            xhr.setRequestHeader('Content-Type', 'application/json');
            xhr.onload = function () {
                if (xhr.status === 200) {
                    var jsonResponse = JSON.parse(xhr.responseText);
                    alert(jsonResponse.message);
                }
            };
            xhr.send(JSON.stringify({
                isHelpful: isHelpful
            }));
        });
    });
}

function handleCommentInteraction() {
    var interactionButtons = document.querySelectorAll('.comment-interaction-button');
    interactionButtons.forEach(function (button) {
        button.addEventListener('click', function () {

            var commentId = this.dataset.commentId;
            var isHelpful = this.dataset.isHelpful === '1';
            var reviewId = this.dataset;
            console.log(reviewId);

            var xhr = new XMLHttpRequest();
            xhr.open('POST', '/advanced-review/comment-interaction/' + commentId, true);
            xhr.setRequestHeader('Content-Type', 'application/json');
            xhr.onload = function () {
                if (xhr.status === 200) {
                    var jsonResponse = JSON.parse(xhr.responseText);
                    alert(jsonResponse.message);
                }
            };
            xhr.send(JSON.stringify({
                isHelpful: isHelpful,
                reviewId: reviewId,
            }));
        });
    });
}

document.addEventListener('DOMContentLoaded', function () {
    handleReviewInteraction();
    handleCommentInteraction();
});