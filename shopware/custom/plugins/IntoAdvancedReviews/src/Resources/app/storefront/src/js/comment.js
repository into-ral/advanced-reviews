function handleCommentFormDisplay() {
    console.log('Comment Script loaded');
    var commentButtons = document.querySelectorAll('.comment-button');
    commentButtons.forEach(function (button) {
        button.addEventListener('click', function (e) {
            var form = e.target.nextElementSibling;
            if (form.style.display === 'none' || form.style.display === '') {
                form.style.display = 'block';
            } else {
                form.style.display = 'none';
            }
        });
    });
}

function handleEditCommentFormDisplay() {
    console.log('Edit Comment Script loaded');
    var editCommentButtons = document.querySelectorAll('.edit-comment-button');
    editCommentButtons.forEach(function(button) {
        button.addEventListener('click', function() {
            var commentId = this.dataset.commentId;
            var form = document.getElementById('edit-comment-form-' + commentId);
            if (form.style.display === 'none' || form.style.display === '') {
                form.style.display = 'block';
            } else {
                form.style.display = 'none';
            }
        });
    });
}

document.addEventListener('DOMContentLoaded', function () {
    handleCommentFormDisplay();
    handleEditCommentFormDisplay();
});