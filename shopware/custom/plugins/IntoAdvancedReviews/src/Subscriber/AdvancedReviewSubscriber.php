<?php declare(strict_types=1);

namespace IntoAdvancedReviews\Subscriber;

use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\MultiFilter;
use Shopware\Core\Framework\Struct\ArrayEntity;
use Shopware\Storefront\Page\Product\ProductPageLoadedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Shopware\Core\Framework\Context;

class AdvancedReviewSubscriber implements EventSubscriberInterface
{
    private EntityRepository $productReviewRepository;
    private EntityRepository $productReviewCommentRepository;

    public function __construct(EntityRepository $productReviewRepository, EntityRepository $productReviewCommentRepository)
    {
        $this->productReviewRepository = $productReviewRepository;
        $this->productReviewCommentRepository = $productReviewCommentRepository;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            ProductPageLoadedEvent::class => 'onProductPageLoaded',
        ];
    }

    public function onProductPageLoaded(ProductPageLoadedEvent $event): void
    {
        $productId = $event->getPage()->getProduct()->getId();
        $customer = $event->getSalesChannelContext()->getCustomer();

        $reviews = $this->getReviewsByProductId($productId);
        $canReview = false;

        if ($customer) {
            $customerId = $customer->getId();
            $canReview = !$this->checkIfReviewExists($productId, $customerId);
        }


        foreach ($reviews as $review) {
            $context = $event->getContext();
            $reviewId = $review->getId();

            $mainComments = $this->getAndSortComments($reviewId, $context);

            $review->addExtension('comments', $mainComments);
        }

        $event->getPage()->addExtension('advanced_reviews_data', new ArrayEntity([
            'reviews' => $reviews,
            'canReview' => $canReview,
        ]));
    }

    private function getReviewsByProductId(string $productId): array
    {
        $criteria = new Criteria();
        $criteria->addFilter(new EqualsFilter('productId', $productId));
        $criteria->addFilter(new EqualsFilter('isApproved', 1));
        $reviews = $this->productReviewRepository->search($criteria, Context::createDefaultContext());
        return $reviews->getElements();
    }

    private function checkIfReviewExists(string $productId, string $customerId): bool
    {
        $criteria = new Criteria();
        $criteria->addFilter(new MultiFilter(MultiFilter::CONNECTION_AND, [
            new EqualsFilter('productId', $productId),
            new EqualsFilter('customerId', $customerId),
        ]));

        $reviews = $this->productReviewRepository->search($criteria, Context::createDefaultContext());

        return $reviews->count() > 0;
    }

    private function getAndSortComments($reviewId, $context): \Shopware\Core\Framework\DataAbstractionLayer\Search\EntitySearchResult
    {
        $criteria = new Criteria();
        $criteria->addFilter(new EqualsFilter('reviewId', $reviewId));
        $comments = $this->productReviewCommentRepository->search($criteria, $context)->getElements();

        $criteria = new Criteria();
        $criteria->addFilter(new EqualsFilter('referenceCommentId', null));
        $criteria->addFilter(new EqualsFilter('reviewId', $reviewId));
        $mainComments = $this->productReviewCommentRepository->search($criteria, $context);
        $subComments = null;
        foreach ($mainComments as $mainComment) {
            $criteria = new Criteria();
            $criteria->addFilter(new EqualsFilter('referenceCommentId', $mainComment->get('id')));
            $subComments = $this->productReviewCommentRepository->search($criteria, $context);
            $mainComment->addExtension('subComments', $subComments);
        }

        if ($subComments != null) {
            $this->fetchAndAttachSubComments($subComments->getElements(), $context);
        }

        return $mainComments;
    }

    private function fetchAndAttachSubComments($parentComments, $context) : void
    {
        foreach ($parentComments as $parentComment) {
            if ($parentComment) {
                $subCommentCriteria = new Criteria();
                $subCommentCriteria->addFilter(new EqualsFilter('referenceCommentId', $parentComment->get('id')));

                $subCommentsForParent = $this->productReviewCommentRepository->search($subCommentCriteria, $context);
                $parentComment->addExtension('subComments', $subCommentsForParent);
                $this->fetchAndAttachSubComments($subCommentsForParent->getElements(), $context);
            }
        }
    }
}