<?php declare(strict_types=1);

namespace IntoAdvancedReviews\Entity;

use Shopware\Core\Framework\DataAbstractionLayer\EntityCollection;

class InteractionCollection extends EntityCollection
{
    protected function getExpectedClass(): string
    {
        return InteractionEntity::class;
    }
}