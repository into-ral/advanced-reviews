<?php declare(strict_types=1);

namespace IntoAdvancedReviews\Entity;

use Shopware\Core\Checkout\Customer\CustomerDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\EntityDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\Field\BoolField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\CreatedAtField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\FkField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\PrimaryKey;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\Required;
use Shopware\Core\Framework\DataAbstractionLayer\Field\IdField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\ManyToOneAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\UpdatedAtField;
use Shopware\Core\Framework\DataAbstractionLayer\FieldCollection;

class InteractionDefinition extends EntityDefinition
{
    public const ENTITY_NAME = 'into_advanced_product_review_interaction';

    public function getEntityName(): string
    {
        return self::ENTITY_NAME;
    }

    public function getCollectionClass(): string
    {
        return InteractionCollection::class;
    }

    public function getEntityClass(): string
    {
        return InteractionEntity::class;
    }

    protected function defineFields(): FieldCollection
    {
        return new FieldCollection([
            (new IdField('id', 'id'))->addFlags(new PrimaryKey()),
            (new FkField('review_id', 'reviewId', ReviewDefinition::class))->addFlags(new Required()),
            (new FkField('customer_id', 'customerId', CustomerDefinition::class))->addFlags(new Required()),
            (new BoolField('is_helpful', 'isHelpful'))->addFlags(new Required()),
            (new FkField('comment_id', 'commentId', CommentDefinition::class)),
            (new CreatedAtField()),
            (new UpdatedAtField()),
            new ManyToOneAssociationField('review', 'review_id', ReviewDefinition::class, 'id', false),
            new ManyToOneAssociationField('customer', 'customer_id', CustomerDefinition::class, 'id', false),
            new ManyToOneAssociationField('comment', 'comment_id', CommentDefinition::class, 'id', false),
        ]);
    }
}