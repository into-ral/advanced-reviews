<?php declare(strict_types=1);

namespace IntoAdvancedReviews\Entity;

use Shopware\Core\Framework\DataAbstractionLayer\Entity;
use Shopware\Core\Framework\DataAbstractionLayer\EntityIdTrait;
use Shopware\Core\System\Language\LanguageEntity;

class ReviewEntity extends Entity
{
    use EntityIdTrait;

    /**
     * @var int
     */
    /**
     * @var string|null
     */
    protected ?string $productId;
    /**
     * @var string|null
     */
    protected ?string $customerId;
    /**
     * @var LanguageEntity|null
     */
    protected ?LanguageEntity $language;
    /**
     * @var int
     */
    protected int $rating;

    /**
     * @var int
     */
    protected int $migratedProductId;
    /**
     * @var int
     */
    protected int $migratedReviewId;
    /**
     * @var string
     */
    protected string $headline;
    /**
     * @var string
     */
    protected string $text;
    /**
     * @var string
     */
    protected string $alias;
    /**
     * @var string|null
     */
    protected ?string $media;
    /**
     * @var bool
     */
    protected bool $wordFilter;
    /**
     * @var bool
     */
    protected bool $isApproved;
    /**
     * @var int
     */
    protected int $totalInteractions;


    /**
     * @return int
     */
    public function getRating(): int
    {
        return $this->rating;
    }

    /**
     * @param int $rating
     */
    public function setRating(int $rating): void
    {
        $this->rating = $rating;
    }

    /**
     * @return string
     */
    public function getHeadline(): string
    {
        return $this->headline;
    }

    /**
     * @param string $headline
     */
    public function setHeadline(string $headline): void
    {
        $this->headline = $headline;
    }

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText(string $text): void
    {
        $this->text = $text;
    }

    /**
     * @return string
     */
    public function getAlias(): string
    {
        return $this->alias;
    }

    /**
     * @param string $alias
     */
    public function setAlias(string $alias): void
    {
        $this->alias = $alias;
    }

    /**
     * @return bool
     */
    public function isWordFilter(): bool
    {
        return $this->wordFilter;
    }

    /**
     * @param bool $wordFilter
     */
    public function setWordFilter(bool $wordFilter): void
    {
        $this->wordFilter = $wordFilter;
    }

    /**
     * @return bool
     */
    public function isApproved(): bool
    {
        return $this->isApproved;
    }

    /**
     * @param bool $isApproved
     */
    public function setIsApproved(bool $isApproved): void
    {
        $this->isApproved = $isApproved;
    }

    /**
     * @return LanguageEntity|null
     */
    public function getLanguage(): ?LanguageEntity
    {
        return $this->language;
    }

    /**
     * @param LanguageEntity|null $language
     */
    public function setLanguage(?LanguageEntity $language): void
    {
        $this->language = $language;
    }

    /**
     * @return int
     */
    public function getMigratedProductId(): int
    {
        return $this->migratedProductId;
    }

    /**
     * @param int $migratedProductId
     */
    public function setMigratedProductId(int $migratedProductId): void
    {
        $this->migratedProductId = $migratedProductId;
    }

    /**
     * @return string|null
     */
    public function getMedia(): ?string
    {
        return $this->media;
    }

    /**
     * @param string|null $media
     */
    public function setMedia(?string $media): void
    {
        $this->media = $media;
    }

    /**
     * @return int
     */
    public function getMigratedReviewId(): int
    {
        return $this->migratedReviewId;
    }

    /**
     * @param int $migratedReviewId
     */
    public function setMigratedReviewId(int $migratedReviewId): void
    {
        $this->migratedReviewId = $migratedReviewId;
    }

    /**
     * @return string|null
     */
    public function getProductId(): ?string
    {
        return $this->productId;
    }

    /**
     * @param string|null $productId
     */
    public function setProductId(?string $productId): void
    {
        $this->productId = $productId;
    }

    /**
     * @return string|null
     */
    public function getCustomerId(): ?string
    {
        return $this->customerId;
    }

    /**
     * @param string|null $customerId
     */
    public function setCustomerId(?string $customerId): void
    {
        $this->customerId = $customerId;
    }

    /**
     * @return int
     */
    public function getTotalInteractions(): int
    {
        return $this->totalInteractions;
    }

    /**
     * @param int $totalInteractions
     */
    public function setTotalInteractions(int $totalInteractions): void
    {
        $this->totalInteractions = $totalInteractions;
    }

}