<?php declare(strict_types=1);

namespace IntoAdvancedReviews\Entity;

use Shopware\Core\Checkout\Customer\CustomerEntity;
use Shopware\Core\Framework\DataAbstractionLayer\Entity;
use Shopware\Core\Framework\DataAbstractionLayer\EntityIdTrait;

class CommentEntity extends Entity
{
    use EntityIdTrait;


    /**
     * @var ReviewEntity|null
     */
    protected $review;

    /**
     * @var CustomerEntity|null
     */
    protected $customer;

    /**
     * @var CommentEntity|null
     */
    protected $referenceComment;
    /**
     * @var int
     */
    protected int $reviewId;
    /**
     * @var string|null
     */
    protected ?string $referenceCommentId;
    /**
     * @var string
     */
    protected string $name;
    /**
     * @var string
     */
    protected string $email;
    /**
     * @var string
     */
    protected string $text;
    /**
     * @var bool
     */
    protected bool $isApproved;
    /**
     * @var string
     */
    protected string $media;
    /**
     * @var int
     */
    protected int $totalInteractions;

    /**
     * @return bool
     */
    public function isApproved(): bool
    {
        return $this->isApproved;
    }

    /**
     * @param bool $isApproved
     */
    public function setIsApproved(bool $isApproved): void
    {
        $this->isApproved = $isApproved;
    }

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText(string $text): void
    {
        $this->text = $text;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getReviewId(): int
    {
        return $this->reviewId;
    }

    /**
     * @param int $reviewId
     */
    public function setReviewId(int $reviewId): void
    {
        $this->reviewId = $reviewId;
    }

    /**
     * @return ReviewEntity|null
     */
    public function getReview(): ?ReviewEntity
    {
        return $this->review;
    }

    /**
     * @return CustomerEntity|null
     */
    public function getCustomer(): ?CustomerEntity
    {
        return $this->customer;
    }

    /**
     * @return CommentEntity|null
     */
    public function getReferenceComment(): ?CommentEntity
    {
        return $this->referenceComment;
    }

    /**
     * @return int
     */
    public function getTotalInteractions(): int
    {
        return $this->totalInteractions;
    }

    /**
     * @param int $totalInteractions
     */
    public function setTotalInteractions(int $totalInteractions): void
    {
        $this->totalInteractions = $totalInteractions;
    }

    /**
     * @return string
     */
    public function getMedia(): string
    {
        return $this->media;
    }

    /**
     * @param string $media
     */
    public function setMedia(string $media): void
    {
        $this->media = $media;
    }

    /**
     * @return string|null
     */
    public function getReferenceCommentId(): ?string
    {
        return $this->referenceCommentId;
    }

    /**
     * @param string|null $referenceCommentId
     */
    public function setReferenceCommentId(?string $referenceCommentId): void
    {
        $this->referenceCommentId = $referenceCommentId;
    }
}