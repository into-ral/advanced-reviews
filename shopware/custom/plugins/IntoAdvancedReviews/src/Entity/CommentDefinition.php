<?php

namespace IntoAdvancedReviews\Entity;

use Shopware\Core\Checkout\Customer\CustomerDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\EntityDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\Field\BoolField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\CreatedAtField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\FkField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\IntField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\CascadeDelete;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\PrimaryKey;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\Required;
use Shopware\Core\Framework\DataAbstractionLayer\Field\IdField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\ManyToOneAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\StringField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\UpdatedAtField;
use Shopware\Core\Framework\DataAbstractionLayer\FieldCollection;

class CommentDefinition extends EntityDefinition
{

    public const ENTITY_NAME = 'into_advanced_product_review_comment';

    public function getEntityName(): string
    {
        return self::ENTITY_NAME;
    }

    public function getEntityClass(): string
    {
        return CommentEntity::class;
    }

    public function getCollectionClass(): string
    {
        return CommentCollection::class;
    }

    public function defineFields(): FieldCollection
    {
        return new FieldCollection([
            (new IdField('id', 'id'))->addFlags(new PrimaryKey()),
            (new FkField('review_id', 'reviewId', ReviewDefinition::class))->addFlags(new Required()),
            (new FkField('customer_id', 'customerId', CustomerDefinition::class))->addFlags(new Required()),
            (new StringField('name', 'name'))->addFlags(new Required()),
            (new StringField('email', 'email'))->addFlags(new Required()),
            (new StringField('text', 'text'))->addFlags(new Required()),
            (new StringField('media', 'media')),
            (new IntField('total_interactions', 'totalInteractions'))->addFlags(new Required()),
            new BoolField('is_approved', 'isApproved'),
            (new FkField('reference_comment_id', 'referenceCommentId', self::class))->addFlags(new CascadeDelete()),
            (new CreatedAtField()),
            (new UpdatedAtField()),
            new ManyToOneAssociationField('review', 'review_id', ReviewDefinition::class, 'id', false),
            new ManyToOneAssociationField('customer', 'customer_id', CustomerDefinition::class, 'id', false),
            new ManyToOneAssociationField('referenceComment', 'reference_comment_id', self::class, 'id', false),
        ]);
    }
}