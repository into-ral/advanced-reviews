<?php declare(strict_types=1);

namespace IntoAdvancedReviews\Entity;

use Shopware\Core\Framework\DataAbstractionLayer\Entity;
use Shopware\Core\Framework\DataAbstractionLayer\EntityIdTrait;

class InteractionEntity extends Entity
{
    use EntityIdTrait;

    /**
     * @var string
     */
    protected string $reviewId;
    /**
     * @var string
     */
    protected string $customerId;
    /**
     * @var bool
     */
    protected bool $isHelpful;

    /**
     * @var string|null
     */
    protected ?string $commentId;

    /**
     * @return string
     */
    public function getReviewId(): string
    {
        return $this->reviewId;
    }

    /**
     * @param string $reviewId
     * @return void
     */
    public function setReviewId(string $reviewId): void
    {
        $this->reviewId = $reviewId;
    }

    /**
     * @return string
     */
    public function getCustomerId(): string
    {
        return $this->customerId;
    }

    /**
     * @param string $customerId
     * @return void
     */
    public function setCustomerId(string $customerId): void
    {
        $this->customerId = $customerId;
    }

    /**
     * @return bool
     */
    public function isHelpful(): bool
    {
        return $this->isHelpful;
    }

    /**
     * @param bool $isHelpful
     */
    public function setIsHelpful(bool $isHelpful): void
    {
        $this->isHelpful = $isHelpful;
    }

    /**
     * @return string|null
     */
    public function getCommentId(): ?string
    {
        return $this->commentId;
    }

    /**
     * @param string|null $commentId
     */
    public function setCommentId(?string $commentId): void
    {
        $this->commentId = $commentId;
    }
}