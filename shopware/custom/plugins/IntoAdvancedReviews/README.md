# IntoAdvancedReviews - Bewertungsplugin

## ToDo:
1. [ ] Repository-Klassen für die neuen Entities erstellen, um die Datenbankzugriffe zu verwalten

2. [ ] API-Routen für die neuen Entities erstellen. --> Controller, die die CRUD-Operationen für die neuen Entities über die API verwalten. Verwenden Sie die Repository-Klassen aus Schritt 1, um die Datenbankzugriffe zu verwalten.

3.[ ] Services erstellen, die die Geschäftslogik für das Bewertungsplugin und die zusätzlichen Funktionen implementieren. Diese Services können Methoden enthalten, um Bewertungen zu erstellen, zu aktualisieren, zu löschen, hilfreiche Bewertungen zu verwalten und andere spezifische Anforderungen umzusetzen.

4.[ ] Subscriber-Klassen erstellen, um auf Shopware-Events zu hören und die gewünschten Aktionen auszuführen. Zum Beispiel können Sie einen Subscriber erstellen, der auf das Event `ProductReviewEvents::REVIEW_WRITTEN_EVENT` hört, um zusätzliche Aktionen auszuführen, wenn eine neue Bewertung erstellt wird.

5.[ ] Plugin-Administration erstellen, um die Plugin-Einstellungen und -Funktionalitäten im Shopware-Adminbereich zu verwalten. Dies kann die Anzeige von Bewertungen, das Verwalten von hilfreichen Bewertungen und das Anpassen von Plugin-Einstellungen umfassen.

6.[ ] Anpassen des Frontend-Templates, um die zusätzlichen Funktionen und Informationen in der Storefront anzuzeigen. Erstellen Sie neue Vue.js-Komponenten oder erweitern Sie bestehende Komponenten, um die Bewertungen und hilfreichen Bewertungen anzuzeigen, und ermöglichen Sie den Kunden, hilfreiche Bewertungen zu markieren.

7.[ ] Testen des Plugins, um sicherzustellen, dass alle Funktionen wie erwartet funktionieren und es keine Fehler oder Probleme gibt. 

8.[ ] Dokumentation für das Plugin erstellen





# Doku

### Create-Migration
```bash
  bin/console database:create-migration -p IntoAdvancedReviews --name FileName
```

### Update method:
```bash
bin/console database:migrate IntoAdvancedReviews --all
```

### UpdateDestructive method:
```bash
php bin/console database:migrate-destructive IntoAdvancedReviews --all
```