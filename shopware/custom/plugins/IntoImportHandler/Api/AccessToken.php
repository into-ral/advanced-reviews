<?php

namespace Api;
require 'Credentials.php';
require_once __DIR__ . '/../../../../vendor/autoload.php';

use GuzzleHttp\Client;

class AccessToken
{
    private $accessToken;

    public function __construct()
    {
        $this->getAccessToken();
    }

    private function getAccessToken(): void
    {
        $client = new Client();
        $response = $client->post(HOST . '/api/oauth/token', [
            'json' => [
                'client_id' => 'administration',
                'grant_type' => 'password',
                'scopes' => '',
                'username' => USERNAME,
                'password' => PASSWORD,
            ],
            'headers' => [
                'Content-Type' => 'application/json',
            ],
        ]);

        $body = json_decode($response->getBody(), true);
        $this->accessToken = $body['access_token'];
    }

    public function getAccessTokenValue()
    {
        return $this->accessToken;
    }
}