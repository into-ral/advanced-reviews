<?php

namespace Api;

use PDO;

class DatabaseConnection
{
    public static function getOldDbConnection(): PDO
    {
        return new PDO('mysql:host=127.0.0.1;dbname=oldnarayananeu;charset=utf8', 'root', 'root');
    }
}