<?php

require_once __DIR__ . '/../../../../vendor/autoload.php';

use Shopware\Core\Framework\Uuid\Uuid;

function convertId($idString)
{
    $idString = str_replace('-', '', $idString);

    // Überprüfung, ob Zeichenkette gültige hexadezimale UUID ist
    if (strlen($idString) !== 32 || !ctype_xdigit($idString)) {
        throw new InvalidArgumentException('Ungültige ID-Zeichenkette');
    }

    // Konvertierung der Zeichenkette in 16-Byte-Binärformat
    $idBinary = hex2bin($idString);

    return $idBinary;
}

try {
    // Datenbankverbindungen einrichten
    $oldDb = new PDO('mysql:host=127.0.0.1;dbname=oldnarayananeu;charset=utf8', 'root', 'root');
    $newDb = new PDO('mysql:host=127.0.0.1;dbname=shopware;charset=utf8', 'root', 'root');

    // Überprüfe, welche IDs bereits migriert wurden
    $migratedReviewIds = $newDb->query("SELECT migrated_review_id FROM into_advanced_product_review")->fetchAll(PDO::FETCH_COLUMN);

    // Alte Daten abrufen, die noch nicht migriert wurden
    $query = $oldDb->query("SELECT * FROM comments_posts WHERE id NOT IN (" . implode(',', array_map('intval', $migratedReviewIds)) . ")");
    $oldData = $query->fetchAll(PDO::FETCH_ASSOC);

    // Prepared Statements für die Einfügungen vorbereiten
    $insertReview = $newDb->prepare("INSERT INTO into_advanced_product_review (
        id, product_id, customer_id, language_id, migrated_product_id, migrated_review_id,
        rating, headline, text, alias, media, total_interactions,
        word_filter, is_approved, created_at
    ) VALUES (
        :id, :product_id, :customer_id, :language_id, :migrated_product_id, :migrated_review_id,
        :rating, :headline, :text, :alias, :media, :total_interactions,
        :word_filter, :is_approved, :created_at
    )");

    // Erweitere das INSERT-Statement um die total_interactions Spalte
    $insertReview->bindParam(':total_interactions', $totalInteractions);

    // Prepared Statement für die Aktualisierungen vorbereiten
    $updateReview = $newDb->prepare("UPDATE into_advanced_product_review SET
        rating = :rating, headline = :headline, text = :text, alias = :alias, total_interactions = :total_interactions
        WHERE migrated_review_id = :migrated_review_id");

    // versionId + languageId
    $versionId = '0fa91ce3-e96a-4bc2-be4b-d9ce752c3425';
    $languageId = 'b9ff2f1d-9dab-42a0-8045-b4bb9114e560';
    $createdAt = date('Y-m-d H:i:s');

    foreach ($oldData as $row) {
        $id = Uuid::randomHex();
        $productId = null;
        $customerId = null;
        $totalInteractions = $row['likes_total'];

        // Spezialfall: product_id
        $productQuery = $newDb->prepare("SELECT id FROM product WHERE product_number = :product_number");
        $productQuery->execute([':product_number' => $row['books_id']]);
        $productResult = $productQuery->fetch(PDO::FETCH_ASSOC);
        if ($productResult) {
            $productId = $productResult['id'];
        }

        // Spezialfall: customer_id
        $customerQuery = $newDb->prepare("SELECT id FROM customer WHERE customer_number = :customer_number");
        $customerQuery->execute([':customer_number' => $row['customers_id']]);
        $customerResult = $customerQuery->fetch(PDO::FETCH_ASSOC);
        if ($customerResult) {
            $customerId = $customerResult['id'];
        }

        if ($productId && $customerId) {
            $success = $insertReview->execute([
                ':id' => convertId($id),
                ':product_id' => $productId,
                ':customer_id' => $customerId,
                ':language_id' => convertId($languageId),
                ':migrated_product_id' => $row['books_id'],
                ':migrated_review_id' => $row['id'],
                ':rating' => $row['rating'],
                ':headline' => $row['title'],
                ':text' => $row['comment'],
                ':alias' => $row['fullname'],
                ':media' => null,
                ':word_filter' => null,
                ':is_approved' => 1,
                ':created_at' => $createdAt,
                // ':total_interactions' wird durch bindParam zugewiesen
            ]);

            // Nach dem Execute-Aufruf, prüfe ob die Daten erfolgreich eingefügt wurden
            if ($success) {
                echo "Bewertung erfolgreich migriert (books_id: " . $row['books_id'] . ")\n";
            } else {
                $errorInfo = $insertReview->errorInfo();
                echo "Fehler beim Einfügen der Bewertung (books_id: " . $row['books_id'] . "): " . $errorInfo[2] . "\n";
            }
        } else {
            echo "Produkt oder Kunde nicht gefunden (books_id: " . $row['books_id'] . ", customers_id: " . $row['customers_id'] . ")\n";
        }
    }

    echo "Migration erfolgreich abgeschlossen.\n";
} catch (PDOException $e) {
    echo "Datenbankfehler: " . $e->getMessage() . "\n";
} catch (Exception $e) {
    echo "Allgemeiner Fehler: " . $e->getMessage() . "\n";
}