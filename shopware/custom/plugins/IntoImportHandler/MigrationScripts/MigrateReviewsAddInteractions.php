<?php

require_once __DIR__ . '/../../../../vendor/autoload.php';

use Shopware\Core\Framework\Uuid\Uuid;

function convertId($idString)
{
    $idString = str_replace('-', '', $idString);

    if (strlen($idString) !== 32 || !ctype_xdigit($idString)) {
        throw new InvalidArgumentException('Ungültige ID-Zeichenkette');
    }

    $idBinary = hex2bin($idString);

    return $idBinary;
}

try {
    $oldDb = new PDO('mysql:host=127.0.0.1;dbname=oldnarayananeu;charset=utf8', 'root', 'root');
    $newDb = new PDO('mysql:host=127.0.0.1;dbname=shopware;charset=utf8', 'root', 'root');

    $migratedReviewIds = $newDb->query("SELECT migrated_review_id FROM into_advanced_product_review")->fetchAll(PDO::FETCH_COLUMN);

    $query = $oldDb->query("SELECT id, likes_total FROM comments_posts");
    $oldData = $query->fetchAll(PDO::FETCH_ASSOC);

    $updateReview = $newDb->prepare("UPDATE into_advanced_product_review SET total_interactions = :total_interactions WHERE migrated_review_id = :migrated_review_id");

    foreach ($oldData as $row) {
        $totalInteractions = $row['likes_total'];
        $migratedReviewId = $row['id'];

        $success = $updateReview->execute([
            ':total_interactions' => $totalInteractions,
            ':migrated_review_id' => $migratedReviewId,
        ]);

        if ($success) {
            echo "Total Interactions erfolgreich migriert für migrated_review_id: " . $migratedReviewId . "\n";
        } else {
            if ($updateReview->rowCount() === 0) {
                echo "Keine Zeile aktualisiert für migrated_review_id: " . $migratedReviewId . "\n";
            } else {
                $errorInfo = $updateReview->errorInfo();
                echo "Fehler beim Aktualisieren der Total Interactions für migrated_review_id: " . $migratedReviewId . ": " . $errorInfo[2] . "\n";
            }
        }
    }

    echo "Migration der Total Interactions erfolgreich abgeschlossen.\n";
} catch (PDOException $e) {
    echo "Datenbankfehler: " . $e->getMessage() . "\n";
} catch (Exception $e) {
    echo "Allgemeiner Fehler: " . $e->getMessage() . "\n";
}