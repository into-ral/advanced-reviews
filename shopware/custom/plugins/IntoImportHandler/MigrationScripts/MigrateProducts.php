<?php

namespace MigrationScripts;

use Api\AccessToken;
use Api\DatabaseConnection;
use Exception;
use GuzzleHttp\Client;
use PDO;
use PDOException;
use Shopware\Core\Framework\Uuid\Uuid;

require __DIR__ . '/../Api/AccessToken.php';
require __DIR__ . '/../Api/DatabaseConnection.php';

class MigrateProducts
{
    private $accessToken;

    public function __construct($accessToken)
    {
        $this->accessToken = $accessToken;
    }

    public function migrate()
    {


        // Daten festlegen
        $taxId = '43023908bb504776bc0b8f76d575e760';
        $currencyId = 'b7d2554b0ce847cd82f3ac9bd1c0dfca';

        // Datenbankverbindung einrichten
        $oldDb = DatabaseConnection::getOldDbConnection();

        // Alte Daten abrufen
        $query = $oldDb->query("SELECT books_id, books_name_substring, books_price_EUR FROM books");
        $oldData = $query->fetchAll(PDO::FETCH_ASSOC);

        // Shopware 6 API-URL und Zugangsdaten
        $apiUrl = HOST . '/api/';

        // Guzzle-Client für API-Anfragen erstellen
        $client = new Client([
            'base_uri' => $apiUrl,
            'headers' => [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . $this->accessToken,
            ],
        ]);

        $createdAt = date('Y-m-d\TH:i:sP');

        echo "Anzahl der abgerufenen Datensätze: " . count($oldData) . "\n";
        echo "API-URL: " . $apiUrl . "\n";
        foreach ($oldData as $row) {
// Neue Produkt-ID erstellen
            $productId = Uuid::randomHex();

// Preisberechnung
            $netPrice = floatval($row['books_price_EUR']);
            $grossPrice = round($netPrice * 1.19);

            echo "Migriere Produkt: {$row['books_id']} - {$row['books_name_substring']}\n";

// Produkt-Array für API-Anfrage erstellen
            $productData = [
                'id' => $productId,
                'name' => $row['books_name_substring'],
                'productNumber' => (string)$row['books_id'],
                'taxId' => $taxId,
                'stock' => 1,
                'price' => [
                    [
                        'currencyId' => $currencyId,
                        'net' => $netPrice,
                        'gross' => $grossPrice,
                        'linked' => true,
                    ],
                ],
                'active' => true,
                'createdAt' => $createdAt,
            ];
            echo "API-Anfrage: " . json_encode($productData) . "\n";

// API-Anfrage zum Erstellen des Produkts
            $response = $client->post('product/', [
                'json' => $productData,
            ]);

            $responseData = json_decode($response->getBody(), true);

            echo "API-Antwort: " . json_encode($responseData) . "\n";

        }

        echo "Migration erfolgreich abgeschlossen.\n";
    }
}

try {
    $accessToken = new AccessToken();
    $migrateProducts = new MigrateProducts($accessToken->getAccessTokenValue());
    $migrateProducts->migrate();
} catch (PDOException $e) {
    echo "Datenbankfehler: " . $e->getMessage() . "\n";
} catch (Exception $e) {
    echo "Allgemeiner Fehler: " . $e->getMessage() . "\n";
}