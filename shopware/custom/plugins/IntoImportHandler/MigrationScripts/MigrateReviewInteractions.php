<?php

require_once __DIR__ . '/../../../../vendor/autoload.php';

use Shopware\Core\Framework\Uuid\Uuid;

function convertId($idString)
{
    $idString = str_replace('-', '', $idString);

    if (strlen($idString) !== 32 || !ctype_xdigit($idString)) {
        throw new InvalidArgumentException('Ungültige ID-Zeichenkette');
    }

    $idBinary = hex2bin($idString);

    return $idBinary;
}

try {
    $oldDb = new PDO('mysql:host=127.0.0.1;dbname=oldnarayananeu;charset=utf8', 'root', 'root');
    $newDb = new PDO('mysql:host=127.0.0.1;dbname=shopware;charset=utf8', 'root', 'root');

    $oldDb->exec("DELETE FROM comments_likes WHERE val != 1 AND val != -1");

    $query = $oldDb->query("SELECT * FROM comments_likes");
    $oldData = $query->fetchAll(PDO::FETCH_ASSOC);

    $insertInteraction = $newDb->prepare("INSERT INTO into_advanced_product_review_interaction (
        id, review_id, comment_id, customer_id, is_helpful, created_at
    ) VALUES (
        :id, :review_id, :comment_id, :customer_id, :is_helpful, :created_at
    )");

    $createdAt = date('Y-m-d H:i:s');

    $commentQuery = $newDb->query("SELECT id, migrated_comment_id FROM into_advanced_product_review_comment");
    $commentsData = $commentQuery->fetchAll(PDO::FETCH_ASSOC);
    $oldIdToNewIdMap = [];

    foreach ($commentsData as $comment) {
        $oldIdToNewIdMap[$comment['migrated_comment_id']] = $comment['id'];
    }

    foreach ($oldData as $row) {
        $id = Uuid::randomHex();
        $reviewId = null;
        $commentId = null;
        $customerId = null;

        $postQuery = $oldDb->prepare("SELECT * FROM comments_posts WHERE id = :post_id");
        $postQuery->execute([':post_id' => $row['post_id']]);
        $postResult = $postQuery->fetch(PDO::FETCH_ASSOC);
        echo "post_id: " . $row['post_id'] . ", parent_id: " . $postResult['parent_id'] . "\n";


        if ($postResult) {
            if ($postResult['parent_id'] == '0') {
                // Es handelt sich um eine Bewertung
                $reviewQuery = $newDb->prepare("SELECT id FROM into_advanced_product_review WHERE migrated_product_id = :migrated_product_id");
                $reviewQuery->execute([':migrated_product_id' => $postResult['books_id']]);
                $reviewResult = $reviewQuery->fetch(PDO::FETCH_ASSOC);
                if ($reviewResult) {
                    $reviewId = $reviewResult['id'];
                }
            }
            else {
                // Es handelt sich um einen Kommentar
                if (isset($oldIdToNewIdMap[$postResult['id']])) {
                    $commentId = $oldIdToNewIdMap[$postResult['id']];
                } else {
                    echo "Kommentar nicht gefunden (post_id: " . $postResult['id'] . ")\n";
                    echo "Nicht gefundener Kommentar: post_id=" . $postResult['id'] . ", parent_id=" . $postResult['parent_id'] . ", books_id=" . $postResult['books_id'] . "\n";

                }
            }

            $customerQuery = $newDb->prepare("SELECT HEX(id) as id FROM customer WHERE CAST(customer_number AS UNSIGNED) = :customer_number");
            $customerQuery->execute([':customer_number' => (string)$postResult['customers_id']]);
            $customerResult = $customerQuery->fetch(PDO::FETCH_ASSOC);
            if ($customerResult) {
                $customerId = convertId($customerResult['id']);
            }
        }

        if ($customerId && ($reviewId || $commentId)) {
            $success = $insertInteraction->execute([
                ':id' => convertId($id),
                ':review_id' => $reviewId,
                ':comment_id' => $commentId,
                ':customer_id' => $customerId,
                ':is_helpful' => $row['val'],
                ':created_at' => $createdAt,
            ]);

            if (!$success) {
                $errorInfo = $insertInteraction->errorInfo();
                echo "Fehler beim Einfügen der Interaktion (post_id: " . $row['post_id'] . "): " . $errorInfo[2] . "\n";
            }
        } else {
            echo "Kunde nicht gefunden (customers_id: " . ($postResult ? $postResult['customers_id'] : '') . ", post_id: " . $row['post_id'] . ")\n";
        }
    }

    echo "Migration erfolgreich abgeschlossen.\n";

} catch (PDOException $e) {
    echo "Datenbankfehler: " . $e->getMessage() . "\n";
} catch (Exception $e) {
    echo "Allgemeiner Fehler: " . $e->getMessage() . "\n";
}