<?php

namespace MigrationScripts;

use Api\AccessToken;
use Api\DatabaseConnection;
use Exception;
use GuzzleHttp\Client;
use PDO;
use Shopware\Core\Framework\Uuid\Uuid;

require __DIR__ . '/../Api/AccessToken.php';
require __DIR__ . '/../Api/DatabaseConnection.php';

class MigrateCustomers
{
    private $accessToken;

    public function __construct($accessToken)
    {
        $this->accessToken = $accessToken;
    }

    public function migrate()
    {

        // Daten festlegen
        $salesChannelId = '95c7ea7e42e847b5add2cd9e02ba7a85';
        $defaultPaymentMethodId = '2d710608a0644021b4e975e99f73b7f1';
        $salutationId = '9a279394c9db43e8ada01c4d77e7835d';
        $languageId = 'b9ff2f1d9dab42a08045b4bb9114e560';
        $countryId = '38313d2e451f4bc0a6df079d661f2b3f';


        // Datenbankverbindung einrichten
        $oldDb = DatabaseConnection::getOldDbConnection();
        $newDb = new PDO('mysql:host=127.0.0.1;dbname=shopware;charset=utf8', 'root', 'root');

        // Alte Daten abrufen
        $query = $oldDb->query("SELECT customers_id, customers_firstname, customers_lastname, customers_password, customers_email_address, customers_newsletter, entry_company, entry_street_address, entry_postcode, entry_city, customers_telephone FROM customers");
        $oldData = $query->fetchAll(PDO::FETCH_ASSOC);


        // Shopware 6 API-URL und Zugangsdaten
        $apiUrl = HOST . '/api/';

        // Guzzle-Client für API-Anfragen erstellen
        $client = new Client([
            'base_uri' => $apiUrl,
            'headers' => [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . $this->accessToken,
            ],
        ]);

        $createdAt = date('Y-m-d\TH:i:sP');

        echo "Anzahl der abgerufenen Datensätze: " . count($oldData) . "\n";
        echo "API-URL: " . $apiUrl . "\n";
        foreach ($oldData as $row) {
            // Prüfen, ob die Kunden-ID bereits migriert wurde
            $checkQuery = $newDb->prepare("SELECT * FROM customer WHERE customer_number = :customers_id");
            $checkQuery->execute([':customers_id' => $row['customers_id']]);
            $existingCustomer = $checkQuery->fetch(PDO::FETCH_ASSOC);

            if ($existingCustomer) {
                echo "Kunde {$row['customers_id']} - {$row['customers_firstname']} {$row['customers_lastname']} wurde bereits migriert, überspringe.\n";
                continue;
            }
            // Neue Kunden-ID erstellen
            $customerId = Uuid::randomHex();
            $addressId = Uuid::randomHex();

            // Kunden-Array für API-Anfrage erstellen
            $customerData = [
                'id' => $customerId,
                'customerNumber' => (string)$row['customers_id'],
                'salutationId' => $salutationId,
                'firstName' => $row['customers_firstname'],
                'lastName' => $row['customers_lastname'],
                'password' => $row['customers_password'],
                'email' => $row['customers_email_address'],
                'birthday' => null,
                'createdAt' => $createdAt,
                'newsletter' => (bool)$row['customers_newsletter'],
                'defaultBillingAddressId' => $addressId,
                'defaultShippingAddressId' => $addressId,
                'defaultPaymentMethodId' => $defaultPaymentMethodId,
                'group' => [
                    'id' => 'cfbd5018d38d41d8adca10d94fc8bdd6',
                ],
                'salesChannelId' => $salesChannelId,
                'languageId' => $languageId,
                'defaultBillingAddress' => [
                    'id' => $addressId,
                    'salutationId' => $salutationId,
                    'firstName' => $row['customers_firstname'],
                    'lastName' => $row['customers_lastname'],
                    'street' => $row['entry_street_address'],
                    'zipcode' => $row['entry_postcode'],
                    'city' => $row['entry_city'],
                    'phoneNumber' => $row['customers_telephone'],
                    'company' => $row['entry_company'],
                    'countryId' => $countryId,
                ],
                'defaultShippingAddress' => [
                    'id' => $addressId,
                    'salutationId' => $salutationId,
                    'firstName' => $row['customers_firstname'],
                    'lastName' => $row['customers_lastname'],
                    'street' => $row['entry_street_address'],
                    'zipcode' => $row['entry_postcode'],
                    'city' => $row['entry_city'],
                    'phoneNumber' => $row['customers_telephone'],
                    'company' => $row['entry_company'],
                    'countryId' => $countryId,
                ],
            ];

            echo "Migriere Kunde: {$row['customers_id']} - {$row['customers_firstname']} {$row['customers_lastname']}\n";

            // API-Anfrage zum Erstellen des Kunden
            try {
                $response = $client->post('customer/', [
                    'json' => $customerData,
                ]);

                $responseData = json_decode($response->getBody(), true);

                echo "API-Antwort: " . json_encode($responseData) . "\n";
            } catch (Exception $e) {
                $errorDetails = json_decode($e->getResponse()->getBody(), true);
                echo "Fehlermeldung: " . json_encode($errorDetails) . "\n";


                // Überprüfen, ob der Fehler auf einen abgelaufenen Token hinweist
                if ($errorDetails['errors'][0]['code'] == '9') {
                    // Token erneuern
                    $accessToken = new AccessToken();
                    $this->accessToken = $accessToken->getAccessTokenValue();

                    // Anfrage erneut versuchen
                    $client = new Client([
                        'base_uri' => $apiUrl,
                        'headers' => [
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                            'Authorization' => 'Bearer ' . $this->accessToken,
                        ],
                    ]);

                    $response = $client->post('customer/', [
                        'json' => $customerData,
                    ]);

                    $responseData = json_decode($response->getBody(), true);
                    echo "API-Antwort: " . json_encode($responseData) . "\n";
                }
            }

        }
    }
}

try {
    $accessToken = new AccessToken();
    $migrateCustomers = new MigrateCustomers($accessToken->getAccessTokenValue());
    $migrateCustomers->migrate();
    echo 'Erfolgreich!';
} catch (Exception $e) {
    echo "Allgemeiner Fehler: " . $e->getMessage() . "\n";
}

