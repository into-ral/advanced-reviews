<?php

require_once __DIR__ . '/../../../../vendor/autoload.php';

use Shopware\Core\Framework\Uuid\Uuid;

function convertId($idString)
{
    $idString = str_replace('-', '', $idString);

    if (strlen($idString) !== 32 || !ctype_xdigit($idString)) {
        throw new InvalidArgumentException('Ungültige ID-Zeichenkette');
    }

    $idBinary = hex2bin($idString);

    return $idBinary;
}

try {
    $oldDb = new PDO('mysql:host=127.0.0.1;dbname=oldnarayananeu;charset=utf8', 'root', 'root');
    $newDb = new PDO('mysql:host=127.0.0.1;dbname=shopware;charset=utf8', 'root', 'root');

    $query = $oldDb->query("SELECT * FROM comments_posts");
    $oldData = $query->fetchAll(PDO::FETCH_ASSOC);

    $checkReview = $newDb->prepare("SELECT * FROM into_advanced_product_review_comment WHERE migrated_comment_id = :migrated_review_id");
    $insertReview = $newDb->prepare("INSERT INTO into_advanced_product_review_comment (
        id, customer_id, language_id, migrated_review_id,
        rating, headline, text, alias, media,
        word_filter, is_approved, created_at
    ) VALUES (
        :id, :customer_id, :language_id, :migrated_review_id,
        :rating, :headline, :text, :alias, :media,
        :word_filter, :is_approved, :created_at
    )");

    $versionId = '0fa91ce3-e96a-4bc2-be4b-d9ce752c3425';
    $languageId = 'b9ff2f1d-9dab-42a0-8045-b4bb9114e560';
    $createdAt = date('Y-m-d H:i:s');
    $missingCustomers = [];

    foreach ($oldData as $row) {
        $checkReview->execute([':migrated_review_id' => $row['id']]);
        $reviewExists = $checkReview->fetch(PDO::FETCH_ASSOC);

        if (!$reviewExists) {
            $id = Uuid::randomHex();
            $productId = null;
            $customerId = null;

            $customerQuery = $newDb->prepare("SELECT id FROM customer WHERE customer_number = :customer_number");
            $customerQuery->execute([':customer_number' => $row['customers_id']]);
            $customerResult = $customerQuery->fetch(PDO::FETCH_ASSOC);
            if ($customerResult) {
                $customerId = $customerResult['id'];
            }

            if ($productId && $customerId) {
                $success = $insertReview->execute([
                    ':id' => convertId($id),
                    ':customer_id' => $customerId,
                    ':language_id' => convertId($languageId),
                    ':migrated_review_id' => $row['id'],
                    ':rating' => $row['rating'],
                    ':headline' => $row['title'],
                    ':text' => $row['comment'],
                    ':alias' => $row['fullname'],
                    ':media' => null,
                    ':word_filter' => null,
                    ':is_approved' => 1,
                    ':created_at' => $row['dat'],
                ]);

                if (!$success) {
                    $errorInfo = $insertReview->errorInfo();
                    echo "Fehler beim Einfügen der Bewertung (books_id: " . $row['books_id'] . "): " . $errorInfo[2] . "\n";
                }
            } else {
                echo "Produkt oder Kunde nicht gefunden (books_id: " . $row['books_id'] . ", customers_id: " . $row['customers_id'] . ")\n";
                $missingCustomers[] = $row['customers_id'];
            }
        }
    }

    echo "Migration erfolgreich abgeschlossen.\n";

    if (!empty($missingCustomers)) {
        file_put_contents('missing_customers.txt', implode(',', $missingCustomers));
    }
} catch (PDOException $e) {
    echo "Datenbankfehler: " . $e->getMessage() . "\n";
} catch (Exception $e) {
    echo "Allgemeiner Fehler: " . $e->getMessage() . "\n";
}