<?php

require_once __DIR__ . '/../../../../vendor/autoload.php';

use Shopware\Core\Framework\Uuid\Uuid;

function convertId($idString)
{
    $idString = str_replace('-', '', $idString);

    // Überprüfung, ob Zeichenkette gültige hexadezimale UUID ist
    if (strlen($idString) !== 32 || !ctype_xdigit($idString)) {
        throw new InvalidArgumentException('Ungültige ID-Zeichenkette');
    }

    // Konvertierung der Zeichenkette in 16-Byte-Binärformat
    $idBinary = hex2bin($idString);

    return $idBinary;
}

try {
    // Datenbankverbindungen einrichten
    $oldDb = new PDO('mysql:host=127.0.0.1;dbname=oldnarayananeu;charset=utf8', 'root', 'root');
    $newDb = new PDO('mysql:host=127.0.0.1;dbname=shopware;charset=utf8', 'root', 'root');

    // Alte Daten abrufen
    $query = $oldDb->query("SELECT * FROM comments_posts");
    $oldData = $query->fetchAll(PDO::FETCH_ASSOC);

    // Prepared Statements für die Einfügungen vorbereiten
    $insertComment = $newDb->prepare("INSERT INTO into_advanced_product_review_comment (
        id, review_id, customer_id, reference_comment_id, name,
        email, text, is_approved,
        media, total_interactions, created_at, migrated_comment_id
    ) VALUES (
        :id, :review_id, :customer_id, :reference_comment_id, :alias,
        :email, :text, :is_approved,
        :media, :total_interactions, :created_at, :migrated_comment_id
    )");

    $createdAt = date('Y-m-d H:i:s');
    $oldIdToNewIdMap = [];

    foreach ($oldData as $row) {
        // UUID für den neuen Kommentar generieren
        $id = Uuid::randomHex(); // Stellen Sie sicher, dass Sie die richtige Methode zur UUID-Generierung verwenden

        $reviewId = null;
        $customerId = null;
        $referenceCommentId = null;

        // Spezialfall: review_id
        $reviewQuery = $newDb->prepare("SELECT id FROM into_advanced_product_review WHERE migrated_review_id = :migrated_review_id");
        $reviewQuery->execute([':migrated_review_id' => $row['top_id']]);
        $reviewResult = $reviewQuery->fetch(PDO::FETCH_ASSOC);
        if ($reviewResult) {
            $reviewId = $reviewResult['id'];
        }

        // Spezialfall: customer_id
        $customerQuery = $newDb->prepare("SELECT id FROM customer WHERE customer_number = :customer_number");
        $customerQuery->execute([':customer_number' => $row['customers_id']]);
        $customerResult = $customerQuery->fetch(PDO::FETCH_ASSOC);
        if ($customerResult) {
            $customerId = $customerResult['id'];
        }

        // Überprüfen, ob der Kommentar bereits migriert wurde
        $checkComment = $newDb->prepare("SELECT id FROM into_advanced_product_review_comment WHERE migrated_comment_id = :migrated_comment_id");
        $checkComment->execute([':migrated_comment_id' => $row['id']]);
        $checkResult = $checkComment->fetch(PDO::FETCH_ASSOC);

        if (!$checkResult) {
            if ($reviewId && $customerId) {
                $success = $insertComment->execute([
                    ':id' => convertId($id),
                    ':review_id' => $reviewId,
                    ':customer_id' => $customerId,
                    ':reference_comment_id' => null, // Wird später aktualisiert
                    ':alias' => $row['fullname'],
                    ':email' => $row['email'],
                    ':text' => $row['comment'],
                    ':is_approved' => $row['status'],
                    ':media' => $row['images'],
                    ':total_interactions' => $row['likes'],
                    ':created_at' => $createdAt,
                    ':migrated_comment_id' => $row['id'],
                ]);

                if ($success) {
                    $oldIdToNewIdMap[$row['id']] = $id;
                } else {
                    $errorInfo = $insertComment->errorInfo();
                    echo "Fehler beim Einfügen des Kommentars (books_id: " . $row['books_id'] . "): " . $errorInfo[2] . "\n";
                }
            } else {
                echo "Bewertung oder Kunde nicht gefunden (books_id: " . $row['books_id'] . ", customers_id: " . $row['customers_id'] . ")\n";
            }
        }
    }

    // Hier beginnt der neue Code zur Aktualisierung der reference_comment_id
    $newDb->query("CREATE TEMPORARY TABLE temp_reference_comment_id_map (new_comment_id BINARY(16), migrated_parent_id INT(11))");

    $insertTempMap = $newDb->prepare("INSERT INTO temp_reference_comment_id_map (new_comment_id, migrated_parent_id) VALUES (:new_comment_id, :migrated_parent_id)");

    foreach ($oldData as $row) {
        if ($row['parent_id'] != 0 && isset($oldIdToNewIdMap[$row['id']]) && isset($oldIdToNewIdMap[$row['parent_id']])) {
            $success = $insertTempMap->execute([
                ':new_comment_id' => convertId($oldIdToNewIdMap[$row['id']]),
                ':migrated_parent_id' => $row['parent_id'],
            ]);

            if ($success) {
                echo "Erfolgreich in temp_reference_comment_id_map eingefügt: " . $row['id'] . "\n";
            } else {
                $errorInfo = $insertTempMap->errorInfo();
                echo "Fehler beim Einfügen in temp_reference_comment_id_map: " . $errorInfo[2] . "\n";
            }
        }
    }


    $updateReferenceCommentId = $newDb->prepare("UPDATE into_advanced_product_review_comment c1 JOIN temp_reference_comment_id_map temp_map ON c1.id = temp_map.new_comment_id JOIN into_advanced_product_review_comment c2 ON c2.migrated_comment_id = temp_map.migrated_parent_id SET c1.reference_comment_id = c2.id");

    $updateReferenceCommentId->execute();
    $newDb->query("DROP TEMPORARY TABLE temp_reference_comment_id_map");
    echo "Migration erfolgreich abgeschlossen.\n";

} catch (PDOException $e) {
    // Fehlerbehandlung für PDO-Fehler
    echo "PDO-Fehler: " . $e->getMessage() . "\n";
} catch (Exception $e) {
    // Allgemeine Fehlerbehandlung
    echo "Allgemeiner Fehler: " . $e->getMessage() . "\n";
}